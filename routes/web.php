<?php


use App\Http\Controllers\Main\Image\LoadTestImageController;
use App\Http\Controllers\User\Auth\ForgotPasswordController;
use App\Http\Controllers\User\Auth\ResetPasswordController;
use App\Http\Controllers\User\Auth\VerificationController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

Route::get('/', function () {
    return view('welcome');
});



Route::middleware('guest')->group(function ()
{
    Route::get('/login', [App\Http\Controllers\User\Auth\LoginController::class, 'index'])->name('login');
    Route::post('/login', [App\Http\Controllers\User\Auth\LoginController::class, 'login'])->name('login.store');


    Route::get('register', [App\Http\Controllers\User\Auth\RegisterController::class, 'index'])->name('register');
    Route::post('register', [App\Http\Controllers\User\Auth\RegisterController::class, 'register'])->name('register.store');


    Route::get('/forgot-password', [ForgotPasswordController::class, 'show'])->name('password.request');
    Route::post('/forgot-password', [ForgotPasswordController::class, 'sendResetLink'])->name('password.email');

    Route::get('/reset-password/{token}', [ResetPasswordController::class, 'reset'])->name('password.reset');
    Route::post('/reset-password', [ResetPasswordController::class, 'update'])->name('password.update');
});









Route::group(['prefix' => '/image'], function (){
   Route::post('/load_up-test', [LoadTestImageController::class, 'loadUp'])->name('load_up-test');
});











Route::middleware('auth')->group(function ()
{
    Route::get('/logout', [App\Http\Controllers\User\Auth\LoginController::class, 'logout'])->name('logout');

    Route::prefix('/email')->group(function ()
    {
        Route::get('/verify', [VerificationController::class , 'show'])->name('verification.notice');
        Route::get('/verify/{id}/{hash}', [VerificationController::class , 'verify'])->name('verification.verify');
        Route::post('/resend', [VerificationController::class , 'resend'])->name('verification.resend');
    });


    Route::middleware('verified')->group(function ()
    {



    });
});

