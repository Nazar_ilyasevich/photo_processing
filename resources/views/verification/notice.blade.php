<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
<div>
    <h1>Dashboard</h1>

    @if (session('send'))
        <div role="alert">
            A fresh verification link has been sent to your email address.
        </div>
    @endif

    Before proceeding, please check your email for a verification link. If you did not receive the email,
    <form action="{{ route('verification.resend') }}" method="POST" >
        @csrf
        <button type="submit">
            click here to request another
        </button>.
    </form>
</div>
</body>
</html>
