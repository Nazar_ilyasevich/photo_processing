<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
    <h1>main page</h1>

    @auth('web')
        auth
        <a href="{{ route('logout') }}">logout</a>
    @endauth

    @guest('web')
        guest
        <a href="{{ route('register') }}">register</a>
    @endguest


</body>
</html>
