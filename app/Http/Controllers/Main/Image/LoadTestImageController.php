<?php

namespace App\Http\Controllers\Main\Image;

use App\Http\Controllers\Controller;
use App\Http\Requests\Main\Image\TestImageRequest;
use App\Models\ImageDefault;
use Illuminate\Http\Request;

class LoadTestImageController extends Controller
{
    public function loadUp(TestImageRequest $request)
    {
        $property = $request->getPropertiesAndValidateImage();

        $property['name'] = $request->file('image')->store('image_default');

        ImageDefault::query()->create($property);
    }
}
