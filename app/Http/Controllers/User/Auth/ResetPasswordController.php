<?php

namespace App\Http\Controllers\User\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\User\Auth\ResetPasswordRequest;
use App\Models\User;
use Illuminate\Http\Request;

class ResetPasswordController extends Controller
{
    public function reset(string $token)
    {
        return view('auth.reset', compact('token'));
    }


    public function update(ResetPasswordRequest $request)
    {
        $formData = $request->validated();

        $status = User::resetPassword($formData);

        if ($status){
            return redirect()->route('login')->with(['status' => 'password reset successful']);
        }
        return back()->withErrors(['email' => 'error password reset please try again']);
    }
}
