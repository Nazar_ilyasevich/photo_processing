<?php

namespace App\Http\Controllers\User\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\User\Auth\ForgotPasswordRequest;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Password;

class ForgotPasswordController extends Controller
{
    public function show()
    {
        return view('auth.forgot');
    }


    public function sendResetLink(ForgotPasswordRequest $request)
    {
        $email = $request->validated();
        Password::sendResetLink($email);


        if (Password::RESET_LINK_SENT){
            return back()->with('status', 'email is send');
        }

        return back()->withInput($email)->withErrors(['email' => 'error send']);
    }
}
