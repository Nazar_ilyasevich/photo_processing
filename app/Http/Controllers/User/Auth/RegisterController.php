<?php

namespace App\Http\Controllers\User\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\User\Auth\LoginRequest;
use App\Http\Requests\User\Auth\RegisterRequest;
use App\Models\User;
use App\Providers\RouteServiceProvider;
use Illuminate\Auth\Events\Registered;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class RegisterController extends Controller
{
    public function index()
    {
        return view('register');
    }



    public function register(RegisterRequest $request): RedirectResponse
    {
        $formData = $request->validated();

        $user = User::create($formData, $request->boolean('remember'));

        event(new Registered($user));

        Auth::login($user);

        return redirect()->route('verification.notice');
    }
}
