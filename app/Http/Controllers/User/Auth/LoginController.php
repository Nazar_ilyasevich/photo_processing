<?php

namespace App\Http\Controllers\User\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\User\Auth\LoginRequest;
use App\Providers\RouteServiceProvider;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Illuminate\Validation\ValidationException;

class LoginController extends Controller
{
    public function index()
    {
        return view('login');
    }


    public function login(LoginRequest $request): RedirectResponse
    {
        $request->authorizeAndValidate();

        Session::regenerate();// регенерація сесій

        return redirect()->intended(RouteServiceProvider::HOME);
    }


    public function logout(): RedirectResponse
    {
        Auth::logout();

        Session::invalidate();// скасовує дію поточної сесії
        Session::regenerateToken();// оновлює токен сесії

        return redirect(RouteServiceProvider::HOME);
    }
}
