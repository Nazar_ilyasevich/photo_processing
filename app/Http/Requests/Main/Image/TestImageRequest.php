<?php

namespace App\Http\Requests\Main\Image;

use Illuminate\Foundation\Http\FormRequest;

class TestImageRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\Rule|array|string>
     */
    public function rules(): array
    {
        return [
            'image' => ['required', 'image', 'mimes:jpeg,png,jpg', 'max:5000']// 5 mb
        ];
    }


    public function getPropertiesAndValidateImage(): array
    {
        $this->validated();

        return [
            'type' => $this->file('image')->extension(),
            'size' => $this->file('image')->getSize(),
        ];
    }

}
