<?php

namespace App\Http\Requests\User\Auth;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rules\Password;

class ResetPasswordRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\Rule|array|string>
     */
    public function rules(): array
    {
        return [
            'token'                 => ['required'],
            'email'                 => ['required', 'email', 'string'],
            'password'              => ['required', 'min:8', 'confirmed', Password::default()], //confirmed = password_confirmation
            'password_confirmation' => ['required']
        ];
    }
}
