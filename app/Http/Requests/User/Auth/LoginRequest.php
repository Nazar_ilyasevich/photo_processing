<?php

namespace App\Http\Requests\User\Auth;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\ValidationException;

class LoginRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function  authorize(): bool
    {
        return true;
    }


    public function rules(): array
    {
        return [
            'email'    => ['required', 'email', 'string'],
            'password' => ['required', 'string', 'min:6'],
        ];
    }


    /**
     * @throws ValidationException
     */
    public function authorizeAndValidate(): mixed
    {
        $formData = $this->validated();
        $remember = $this->boolean('remember');


        if (Auth::attempt($formData, $remember)){
            return $formData;
        }

        throw ValidationException::withMessages([
            'message' => 'invalid argument',
        ]);
    }
}
